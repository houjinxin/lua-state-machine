local machine = require('statemachine')
dbg = require('debugger')

local fsm = machine.create({
  initial = 'green',
  events = {
    { name = 'warn',  from = 'green',  to = 'yellow' },
    { name = 'panic', from = 'yellow', to = 'red'    },
    { name = 'calm',  from = 'red',    to = 'yellow' },
    { name = 'clear', from = 'yellow', to = 'green'  }
  },
  -- callbacks = {
  --   onpanic =  function(self, event, from, to, msg) print('panic! ' .. (msg or "nil"))    end,
  --   onclear =  function(self, event, from, to, msg) print('thanks to ' .. (msg or "nil")) end,
  --   ongreen =  function(self, event, from, to)      print('green light')       end,
  --   onyellow = function(self, event, from, to)      print('yellow light')      end,
  --   onred =    function(self, event, from, to)      print('red light')         end,
  -- }
})

fsm.onstatechange = function(self, event, from, to) print(to) end

fsm:warn() -- transition from 'green' to 'yellow'
dbg()
print(fsm.current)
print(fsm.currentTransitioningEvent)
print(fsm:is(fsm.current))
print(fsm:can("warn"))
print(fsm:cannot("warn"))
print("------------------------")

fsm:panic() -- transition from 'yellow' to 'red'
print(fsm.current)
print(fsm.currentTransitioningEvent)
print(fsm:is(fsm.current))
print(fsm:can("panic"))
print(fsm:cannot("panic"))
print("------------------------")

fsm:calm() -- transition from 'red' to 'yellow'
print(fsm.current)
print(fsm.currentTransitioningEvent)
print(fsm:is(fsm.current))
print(fsm:can("calm"))
print(fsm:cannot("calm"))
print("------------------------")

fsm:clear() -- transition from 'yellow' to 'green'
print(fsm.current)
print(fsm.currentTransitioningEvent)
print(fsm:is(fsm.currentn))
print(fsm:can("clear"))
print(fsm:cannot("clear"))
